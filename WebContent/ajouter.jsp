<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1" %>
  <%@ page import="java.util.*" %>
  <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>RVP</title>
      <!-- PACE-->
      <link rel="stylesheet" type="text/css"
        href="plugins/PACE/themes/blue/pace-theme-flash.css">
      <script type="text/javascript" src="plugins/PACE/pace.min.js"></script>
      <!-- Bootstrap CSS-->
      <link rel="stylesheet" type="text/css"
        href="plugins/bootstrap/dist/css/bootstrap.min.css">
      <!-- Fonts-->
      <link rel="stylesheet" type="text/css"
        href="plugins/themify-icons/themify-icons.css">
      <!-- Malihu Scrollbar-->
      <link rel="stylesheet" type="text/css"
        href="plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css">
      <!-- Animo.js-->
      <link rel="stylesheet" type="text/css"
        href="plugins/animo.js/animate-animo.min.css">
      <!-- Flag Icons-->
      <link rel="stylesheet" type="text/css"
        href="plugins/flag-icon-css/css/flag-icon.min.css">
      <!-- Bootstrap Progressbar-->
      <link rel="stylesheet" type="text/css"
        href="plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
      <!-- Weather Icons-->
      <link rel="stylesheet" type="text/css"
        href="plugins/weather-icons/css/weather-icons-wind.min.css">
      <link rel="stylesheet" type="text/css"
        href="plugins/weather-icons/css/weather-icons.min.css">
      <!-- Slick Carousel-->
      <link rel="stylesheet" type="text/css"
        href="plugins/slick-carousel/slick/slick.css">
      <!-- SpinKit-->
      <link rel="stylesheet" type="text/css"
        href="plugins/SpinKit/css/spinners/7-three-bounce.css">
      <!-- Primary Style-->
      <link rel="stylesheet" type="text/css"
        href="build/css/fourth-layout.css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
      <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
      <!--[if lt IE 9]>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>

      <!-- Header start-->
    <jsp:include page="header.jsp" />
     
      <!-- Header end-->
      <div class="main-container">
        <!-- Main Sidebar start-->
        
    <jsp:include page="aside.jsp" />
        <!-- Main Sidebar end-->
        <div class="page-container">
          <div class="page-header container-fluid">
            <div class="row">
              <div class="col-sm-6">
                <h4 class="mt-0 mb-5"></h4>
                <p class="text-muted mb-0">Rechercher votre pharmacie</p>
              </div>
              <div class="col-sm-6">
                <div class="btn-group mt-5">
                   </div>
              </div>
            </div>
          </div>
          <div class="page-content container-fluid">
            
          
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="widget no-border">
                  <div style="height: 200px" class="overlay-container
                    overlay-color"><img src="build/images/backgrounds/34.jpg"
                      alt="" class="overlay-bg img-responsive"></div>
                  <div style="position: relative"><a href="#" style="position:
                      absolute; top: -70px; left: 50%; margin-left: -50px;
                      border-radius: 50%; padding: 3px; background-color: #FFF"><img
                        src="build/images/users/03.jpg" width="100" alt=""
                        class="img-circle"></a></div>
                  <div class="text-center p-20 bd-l bd-r">
                    <h4 class="text-success mt-30 mb-5">Janice Hopkins</h4>
                    <p>Web Developer</p>
                    <p class="mb-0">I am a freelance graphic designer with 20
                      years experience working in the Graphic Design industry.</p>
                  </div>
                  <div class="row row-0 p-15 text-center bg-black">
                    <div class="col-xs-4">
                      <div class="fs-20 fw-500">208</div>
                      <div class="text-muted">Following</div>
                    </div>
                    <div class="col-xs-4">
                      <div class="fs-20 fw-500">560</div>
                      <div class="text-muted">Likes</div>
                    </div>
                    <div class="col-xs-4">
                      <div class="fs-20 fw-500">95</div>
                      <div class="text-muted">Photos</div>
                    </div>
                  </div>
                </div>
               
              
              </div>
             
              <div class="row">

        <!-- Right Sidebar start-->
        <aside class="right-sidebar closed">
          <ul role="tablist" class="nav nav-tabs nav-justified nav-sidebar">
            <li role="presentation" class="active"><a href="#chat"
                aria-controls="chat" role="tab" data-toggle="tab"><i
                  class="ti-comment-alt"></i></a></li>
            <li role="presentation"><a href="#announcement"
                aria-controls="announcement" role="tab" data-toggle="tab"><i
                  class="ti-announcement"></i></a></li>
            <li role="presentation"><a href="#ticket" aria-controls="ticket"
                role="tab" data-toggle="tab"><i class="ti-bookmark-alt"></i></a></li>
            <li role="presentation"><a href="#setting" aria-controls="setting"
                role="tab" data-toggle="tab"><i class="ti-settings"></i></a></li>
          </ul>
          <div data-mcs-theme="minimal-dark" class="tab-content
            nav-sidebar-content mCustomScrollbar">
            <div id="chat" role="tabpanel" class="tab-pane fade in active">
              <form>
                <div class="form-group has-feedback">
                  <input type="text" aria-describedby="inputChatSearch"
                    placeholder="Chat with..." class="form-control rounded"><span
                    aria-hidden="true" class="ti-search form-control-feedback"></span><span
                    id="inputChatSearch" class="sr-only">(default)</span>
                </div>
              </form>
              <ul class="chat-list mb-0 fs-12 media-list">
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/20.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-success"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Jane Curtis</h6>
                      <p class="text-muted mb-0">Where are you from?</p>
                    </div>
                    <div class="media-right"><span class="badge bg-danger">2</span></div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/01.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-success"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Edward Garcia</h6>
                      <p class="text-muted mb-0">Nice to meet you.</p>
                    </div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/02.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-success"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Bruce Olson</h6>
                      <p class="text-muted mb-0">What do you want to do?</p>
                    </div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/03.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-warning"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Martha Rodriguez</h6>
                      <p class="text-muted mb-0">I'm hungry.</p>
                    </div>
                    <div class="media-right"><span class="badge bg-danger">1</span></div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/05.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-success"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Hannah Williamson</h6>
                      <p class="text-muted mb-0">Do you know the address?</p>
                    </div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/06.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-success"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Anthony Mills</h6>
                      <p class="text-muted mb-0">No problem.</p>
                    </div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/07.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-warning"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Ethan Stanley</h6>
                      <p class="text-muted mb-0">Hello?</p>
                    </div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/08.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-success"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Jonathan Castro</h6>
                      <p class="text-muted mb-0">OK. Thanks.</p>
                    </div>
                    <div class="media-right"><span class="badge bg-danger">1</span></div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/09.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-success"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Denise Rose</h6>
                      <p class="text-muted mb-0">Bye bye.</p>
                    </div></a></li>
                <li class="media"><a href="javascript:;"
                    class="conversation-toggle">
                    <div class="media-left avatar"><img
                        src="build/images/users/10.jpg" alt=""
                        class="media-object img-circle"><span class="status
                        bg-danger"></span></div>
                    <div class="media-body">
                      <h6 class="media-heading">Eugene Meyer</h6>
                      <p class="text-muted mb-0">How are you?</p>
                    </div></a></li>
              </ul>
            </div>
            <div id="announcement" role="tabpanel" class="tab-pane fade">
              <ul class="media-list mb-0 fs-12">
                <li class="media">
                  <div class="media-left"><i class="ti-bar-chart-alt
                      media-object mo-xs img-circle bg-primary text-center"></i></div>
                  <div class="media-body">
                    <h6 class="media-heading">Market Trend Data</h6>
                    <p class="text-muted mb-0">Mattis nam fringilla dui nostra,
                      ad fames fusce venenatis massa.</p>
                  </div>
                </li>
                <li class="media">
                  <div class="media-left"><i class="ti-package media-object
                      mo-xs img-circle bg-success text-center"></i></div>
                  <div class="media-body">
                    <h6 class="media-heading">Change Your Password!</h6>
                    <p class="text-muted mb-0">Varius dolor condimentum
                      hendrerit eleifend est urna neque fames faucibus?</p>
                  </div>
                </li>
                <li class="media">
                  <div class="media-left"><i class="ti-gift media-object mo-xs
                      img-circle bg-warning text-center"></i></div>
                  <div class="media-body">
                    <h6 class="media-heading">We Apologize</h6>
                    <p class="text-muted mb-0">Justo at mauris ridiculus conubia
                      penatibus dis varius proin porttitor!</p>
                  </div>
                </li>
                <li class="media">
                  <div class="media-left"><i class="ti-harddrives media-object
                      mo-xs img-circle bg-info text-center"></i></div>
                  <div class="media-body">
                    <h6 class="media-heading">Content Policy Update</h6>
                    <p class="text-muted mb-0">Quis ante imperdiet a volutpat
                      quam tellus condimentum blandit elementum.</p>
                  </div>
                </li>
                <li class="media">
                  <div class="media-left"><i class="ti-mobile media-object mo-xs
                      img-circle bg-purple text-center"></i></div>
                  <div class="media-body">
                    <h6 class="media-heading">Mobile Apps</h6>
                    <p class="text-muted mb-0">Ad iaculis at feugiat integer
                      lobortis vivamus hac egestas venenatis.</p>
                  </div>
                </li>
                <li class="media">
                  <div class="media-left"><i class="ti-alarm-clock media-object
                      mo-xs img-circle bg-danger text-center"></i></div>
                  <div class="media-body">
                    <h6 class="media-heading">New Features</h6>
                    <p class="text-muted mb-0">Primis elementum facilisi
                      tristique faucibus feugiat enim rutrum id himenaeos.</p>
                  </div>
                </li>
              </ul>
            </div>
            <div id="ticket" role="tabpanel" class="tab-pane fade">
              <form>
                <div class="form-group">
                  <input type="text" placeholder="Username"
                    class="form-control">
                </div>
                <div class="form-group">
                  <input type="text" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                  <input type="text" placeholder="Subject" class="form-control">
                </div>
                <div class="form-group">
                  <textarea rows="6" placeholder="Description"
                    class="form-control"></textarea>
                </div>
                <button type="submit" class="btn btn-block btn-success">Create
                  Ticket</button>
              </form>
            </div>
            <div id="setting" role="tabpanel" class="tab-pane fade">
              <form class="form-horizontal fs-12">
                <div class="clearfix">
                  <h6 class="pull-left">Maintenance Mode</h6>
                  <div class="switch pull-right">
                    <input id="setting-1" type="checkbox" checked="">
                    <label for="setting-1" class="switch-success"></label>
                  </div>
                </div>
                <p class="text-muted">Ipsum non tempor non nullam nisi congue
                  nisi amet enim.</p>
                <div class="clearfix">
                  <h6 class="pull-left">Location Services</h6>
                  <div class="switch pull-right">
                    <input id="setting-2" type="checkbox" checked="">
                    <label for="setting-2" class="switch-success"></label>
                  </div>
                </div>
                <p class="text-muted">Eleifend suscipit erat cursus viverra
                  commodo nostra sit commodo mollis.</p>
                <div class="clearfix">
                  <h6 class="pull-left">Display Errors</h6>
                  <div class="switch pull-right">
                    <input id="setting-3" type="checkbox" checked="">
                    <label for="setting-3" class="switch-success"></label>
                  </div>
                </div>
                <p class="text-muted">Amet per tortor adipiscing risus dolor
                  orci diam curabitur senectus.</p>
                <div class="clearfix">
                  <h6 class="pull-left">Use SEO URLs</h6>
                  <div class="switch pull-right">
                    <input id="setting-4" type="checkbox" checked="">
                    <label for="setting-4" class="switch-success"></label>
                  </div>
                </div>
                <p class="text-muted">Ullamcorper dignissim facilisis fames
                  proin a leo diam amet urna.</p>
                <div class="clearfix">
                  <h6 class="pull-left">Enable History</h6>
                  <div class="switch pull-right">
                    <input id="setting-5" type="checkbox" checked="">
                    <label for="setting-5" class="switch-success"></label>
                  </div>
                </div>
                <p class="text-muted">Consectetur cubilia varius vulputate
                  fermentum non dolor cubilia torquent risus.</p>
              </form>
            </div>
          </div>
        </aside>
        <aside class="conversation closed">
          <h5 class="text-center m-0 p-20">Edward Garcia<a href="javascript:;"
              class="conversation-toggle pull-left"><i class="ti-arrow-left
                text-muted"></i></a><a href="javascript:;" class="pull-right"><i
                class="ti-pencil text-muted"></i></a></h5>
          <ul data-mcs-theme="minimal-dark" class="media-list chat-content fs-12
            pl-20 pr-20 mCustomScrollbar">
            <li class="media">
              <div class="media-left avatar"><img
                  src="build/images/users/04.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
              <div class="media-body">
                <p>Hello.</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted">09:45 PM <i class="ti-check text-success"></i></time>
              </div>
            </li>
            <li class="media other">
              <div class="media-body">
                <p>Hi.</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted pull-right">09:46 PM <i class="ti-check
                    text-success"></i></time>
              </div>
              <div class="media-right avatar"><img
                  src="build/images/users/18.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
            </li>
            <li class="media">
              <div class="media-left avatar"><img
                  src="build/images/users/04.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
              <div class="media-body">
                <p>How are you?</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted">09:47 PM <i class="ti-check text-success"></i></time>
              </div>
            </li>
            <li class="media other">
              <div class="media-body">
                <p>I'm good. How are you?</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted pull-right">09:50 PM <i class="ti-check
                    text-success"></i></time>
              </div>
              <div class="media-right avatar"><img
                  src="build/images/users/18.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
            </li>
            <li class="media">
              <div class="media-left avatar"><img
                  src="build/images/users/04.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
              <div class="media-body">
                <p>Good. Do you speak English?</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted">09:55 PM <i class="ti-check text-success"></i></time>
              </div>
            </li>
            <li class="media other">
              <div class="media-body">
                <p>A little. Are you American?</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted pull-right">09:56 PM <i class="ti-check
                    text-success"></i></time>
              </div>
              <div class="media-right avatar"><img
                  src="build/images/users/18.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
            </li>
            <li class="media">
              <div class="media-left avatar"><img
                  src="build/images/users/04.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
              <div class="media-body">
                <p>Yes.</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted">10:00 PM <i class="ti-check text-success"></i></time>
              </div>
            </li>
            <li class="media other">
              <div class="media-body">
                <p>Where are you from?</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted pull-right">10:01 PM <i class="ti-check
                    text-success"></i></time>
              </div>
              <div class="media-right avatar"><img
                  src="build/images/users/18.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
            </li>
            <li class="media">
              <div class="media-left avatar"><img
                  src="build/images/users/04.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
              <div class="media-body">
                <p>I'm from California.</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted">10:03 PM <i class="ti-check text-success"></i></time>
              </div>
            </li>
            <li class="media other">
              <div class="media-body">
                <p>Nice to meet you.</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted pull-right">10:04 PM <i class="ti-check
                    text-success"></i></time>
              </div>
              <div class="media-right avatar"><img
                  src="build/images/users/18.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
            </li>
            <li class="media">
              <div class="media-left avatar"><img
                  src="build/images/users/04.jpg" alt="" class="media-object
                  img-circle"><span class="status bg-success"></span></div>
              <div class="media-body">
                <p>Nice to meet you too.</p>
                <time datetime="2015-12-10T20:50:48+07:00" class="fs-11
                  text-muted">10:05 PM <i class="ti-check text-success"></i></time>
              </div>
            </li>
          </ul>
          <form class="pl-20 pr-20">
            <div class="form-group has-feedback mb-0">
              <input type="text" aria-describedby="inputSendMessage"
                placeholder="Sent a message" class="form-control rounded"><span
                aria-hidden="true" class="ti-pencil-alt form-control-feedback"></span><span
                id="inputSendMessage" class="sr-only">(default)</span>
            </div>
          </form>
        </aside>
        <!-- Right Sidebar end-->
      </div>
      <!-- jQuery-->
      <script type="text/javascript" src="plugins/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap JavaScript-->
      <script type="text/javascript"
        src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Malihu Scrollbar-->
      <script type="text/javascript"
        src="plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
      <!-- Animo.js-->
      <script type="text/javascript" src="plugins/animo.js/animo.min.js"></script>
      <!-- Bootstrap Progressbar-->
      <script type="text/javascript"
        src="plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
      <!-- jQuery Easy Pie Chart-->
      <script type="text/javascript"
        src="plugins/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
      <!-- Sparkline-->
      <script type="text/javascript"
        src="plugins/kapusta-jquery.sparkline/dist/jquery.sparkline.min.js"></script>
      <!-- Flot Charts-->
      <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
      <script type="text/javascript"
        src="plugins/flot/jquery.flot.resize.js"></script>
      <script type="text/javascript"
        src="plugins/flot.curvedlines/curvedLines.js"></script>
      <script type="text/javascript"
        src="plugins/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
      <!-- Slick Carousel-->
      <script type="text/javascript"
        src="plugins/slick-carousel/slick/slick.min.js"></script>
      <!-- jQuery Counter Up-->
      <script type="text/javascript"
        src="plugins/jquery-waypoints/waypoints.min.js"></script>
      <script type="text/javascript"
        src="plugins/Counter-Up/jquery.counterup.min.js"></script>
      <!-- jQuery BlockUI-->
      <script type="text/javascript" src="plugins/blockUI/jquery.blockUI.js"></script>
      <!-- Custom JS-->
      <script type="text/javascript" src="/build/js/fourth-layout/app.js"></script>
      <script type="text/javascript" src="/build/js/fourth-layout/demo.js"></script>
      <script type="text/javascript"
        src="/build/js/page-content/widgets/widgets.js"></script>
    </body>
  </html>
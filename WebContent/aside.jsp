<aside class="main-sidebar">
	<div class="user">
		<div id="esp-user-profile" data-percent="65"
			style="height: 130px; width: 130px; line-height: 100px; padding: 15px;"
			class="easy-pie-chart">
			<img src="build/images/users/04.jpg" alt="" class="avatar img-circle"><span
				class="status
                bg-success"></span>
		</div>
		<h4 class="fs-16 text-white mt-15 mb-5 fw-300">Matthew Gonzalez</h4>
		<p class="mb-0 text-muted">Pharmacien</p>
	</div>
	<ul class="list-unstyled navigation mb-0">
		<li class="sidebar-category">Main</li>
		<li class="panel"><a role="button" data-toggle="collapse"
			data-parent=".navigation" href="index.jsp" aria-expanded="false"
			aria-controls="collapse1" class="bubble
                collapsed"><i
				class="ti-home"></i><span class="sidebar-title">Accueil</span>
		</a> </li>
	</ul>
	<div class="sidebar-widget">
		<ul class="list-unstyled pl-25 pr-25">
			<li class="mb-20">
				<!-- <div class="block clearfix mb-10"><span class="pull-left fs-12
                    text-muted">CPU Used</span><span class="pull-right label
                    label-outline label-warning">65%</span></div>
                <div class="progress progress-xs bg-light mb-0">
                  <div role="progressbar" data-transitiongoal="65"
                    class="progress-bar progress-bar-warning"></div>
                </div> -->
			</li>
			<li class="mb-20">
				<!-- <div class="block clearfix mb-10"><span class="pull-left fs-12
                    text-muted">Bandwidth</span><span class="pull-right label
                    label-outline label-danger">80%</span></div>
                <div class="progress progress-xs bg-light mb-0">
                  <div role="progressbar" data-transitiongoal="80"
                    class="progress-bar progress-bar-danger"></div>
                </div> -->
			</li>
		</ul>
	</div>
</aside>
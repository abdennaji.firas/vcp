 <header>
        <div class="search-bar closed">
          <form>
            <div class="input-group input-group-lg">
              <input type="text" placeholder="Rechercher votre pharmacie"
                class="form-control"><span class="input-group-btn">
                <button type="button" class="btn btn-default search-bar-toggle"><i
                    class="ti-close"></i></button></span>
            </div>
          </form>
        </div><a href="index.html" class="brand pull-left"><img
            src="build/images/logo/logo-light.png" alt="" width="100"
            class="logo"><img src="build/images/logo/logo-sm-light.png"
            alt="" width="28" class="logo-sm"></a><a href="javascript:;"
          role="button" class="hamburger-menu pull-left"><span></span></a>
        <form class="mt-15 mb-15 pull-left hidden-xs">
          <div class="form-group has-feedback mb-0">
            <input type="text" aria-describedby="inputSearchFor"
              placeholder="Search for..." style="width: 200px"
              class="form-control rounded"><span aria-hidden="true"
              class="ti-search form-control-feedback"></span><span
              id="inputSearchFor" class="sr-only">(default)</span>
          </div>
        </form>
        <ul class="notification-bar list-inline pull-right">
          <li class="visible-xs"><a href="javascript:;" role="button"
              class="header-icon search-bar-toggle"><i class="ti-search"></i></a></li>
          
          <li class="dropdown hidden-xs"><a id="dropdownMenu2" href="#"
              data-toggle="dropdown" role="button" aria-haspopup="true"
              aria-expanded="false" class="dropdown-toggle header-icon lh-1
              pt-15 pb-15">
              <div class="media mt-0">
                <div class="media-left avatar"><img
                    src="build/images/users/04.jpg" alt=""
                    class="media-object img-circle"><span class="status
                    bg-success"></span></div>
                <div class="media-right media-middle pl-0">
                  <p class="fs-12 text-base mb-0">Hi, Matthew</p>
                </div>
              </div></a>
            
          </li>
          
        </ul>
      </header>